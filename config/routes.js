/**
 * Route Mappings
 * (sails.config.routes)
 *
 * Your routes tell Sails what to do each time it receives a request.
 *
 * For more information on configuring custom routes, check out:
 * https://sailsjs.com/anatomy/config/routes-js
 */

 const allSubCategory = require("../api/controllers/api/sub_category/all-sub-category");
 const FileController = require("../api/controllers/FileController");
 
 module.exports.routes = {
 
   /***************************************************************************
   *                                                                          *
   * Make the view located at `views/homepage.ejs` your home page.            *
   *                                                                          *
   * (Alternatively, remove this and add an `index.html` file in your         *
   * `assets` directory)                                                      *
   *                                                                          *
   ***************************************************************************/
  
   '/login': { view: 'pages/index', locals: {layout: '/layouts/loginuser.ejs'}},
   '/signup': { view: 'pages/signup' ,locals: {layout: '/layouts/loginuser.ejs'}},
   'GET /category': { action: "api/category/view-add-category" },
   "GET /api/show/:id": { action: "api/category/show-p" },
   "GET /view-category-edit/:id": { action: "api/category/view-update" },
   
   // '/edit_sub_category': { view: 'pages/' ,locals: {layout: '/layouts/loginuser.ejs'}},
   //'/sub_category': { view: 'pages/sub_category' ,locals: {layout: '/layouts/loginuser.ejs'}},
   //'/all_category2': { view: 'pages/sub_category' ,locals: {layout: '/layouts/loginuser.ejs'}},
  //
   "POST /api/user/signup": { action: "api/user/signup" },
   "POST /api/user/login": { action: "api/user/login" },
   "GET /logout": { action: "api/user/logout" },
   
   "POST /api/category/create": { action: "api/category/create" },
   
   "GET /api/category/delete/:id":{action: "api/category/delete"},
   "POST /api/category/update/:id":{action: "api/category/update"},
   "GET /all-category":{action: "api/category/all-category"},
   "GET /sub_category":{action: "api/category/category2"},
   "GET /category-products":{action: "api/category/category-products"},
 
   
  
 
 
   "POST /api/sub_category/create": { action: "api/sub_category/create" },
   "GET /api/sub_category/delete/:id":{action: "api/sub_category/delete"},
   "GET /all-sub-category":{action: "api/sub_category/all-sub-category"},
   "GET /all-products":{action: "api/sub_category/all-products"},
   //"GET /api/sub_category/edit/:id":{action: "api/sub_category/view_edit"},
   "GET /edit/:id": {action: "api/sub_category/view-edit"},
   "POST /edit_sub_category/:id": {action: "api/sub_category/Edit"},
   //"POST api/user/create": {action: "api/controllers/UserController"},
   'POST /api/user/create': 'UserController.create',
   '/user/file':FileController.index,
   '/file/upload':FileController.upload,
 
 
   'GET /buy/:id': {action: "api/user/buy-now"},
   'POST /purchase': {action: "api/user/purchase"},
 
   'GET /add-to-cart/:id': {action: "api/user/add-to-cart"},
   'POST /cart': {action: "api/user/cart"},
 
   'GET /view-cart': {action: "api/user/view-cart"},
   'GET /cart/delete/:id': {action: "api/user/delete-cart-item"},
   'GET /view-orders': {action: "api/user/view-orders"},
   
   
   
   //"/index":{view: "pages/index"}
   // '/profile': {
   //   view: 'pages/profile',
   //   locals: {
   //     category:category.products
   //   }
   // },
    
   /***************************************************************************
   *                                                                          *
   * More custom routes here...                                               *
   * (See https://sailsjs.com/config/routes for examples.)                    *
   *                                                                          *
   * If a request to a URL doesn't match any of the routes in this file, it   *
   * is matched against "shadow routes" (e.g. blueprint routes).  If it does  *
   * not match any of those, it is matched against static assets.             *
   *                                                                          *
   ***************************************************************************/
 
 
 }
 