/**
 * Policy Mappings
 * (sails.config.policies)
 *
 * Policies are simple functions which run **before** your actions.
 *
 * For more information on configuring policies, check out:
 * https://sailsjs.com/docs/concepts/policies
 */

module.exports.policies = {

  /***************************************************************************
  *                                                               *
  * Default policy for all controllers and actions, unless overridden.       *
  * (`true` allows public access)                                            *
  *                                                                          *
  ***************************************************************************/

   //'*': "flash",
 
  "api/user/view-orders": "is-logged-in",
  "api/user/view-orders": "is-user",
  "api/user/view-cart": "is-logged-in",
  "api/user/view-cart": "is-user",
  "api/user/cart": "is-user",
  "api/user/logout": "is-logged-in",
  'api/user/purchase':"is-user",


  "api/category/delete": "is-logged-in",
  "api/category/create": "is-logged-in",
  "api/category/create": "is-seller",
  "api/category/delete": "is-seller",
  "api/category/update": "is-logged-in",
  "api/category/update": "is-seller",

  
  "api/category/view-add-category": "is-logged-in",
  "api/category/view-add-category": "is-seller",
  "api/category/all-category": "is-logged-in",
  "api/category/all-category": "is-seller",
 



 //"api/sub_category/Edit" : "is-user",

};
