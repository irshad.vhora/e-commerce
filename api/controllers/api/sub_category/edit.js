module.exports = {


  friendlyName: 'Edit',


  description: 'Edit sub category.',


  inputs: {

  },


  exits: {

  },


  fn: async function (inputs) {
    try{

        const sub_category = this.req.body  
        
        const old_sub_category = await Sub_category.findOne({ id : this.req.param('id')})

        const sub_categoryRecord = await Sub_category.updateOne({
          id : this.req.param('id')
        }).set({
          category_name : sub_category.category_name?sub_category.category_name:old_sub_category.category_name,
          name :  sub_category.name?sub_category.name:old_sub_category.name,
          model_details :  sub_category.model_details?sub_category.model_details:old_sub_category.model_details,
          description: sub_category.description?sub_category.description:old_sub_category.description,
          photo: sub_category.photo?sub_category.photo:old_sub_category.photo,
          price: sub_category.price?sub_category.price:old_sub_category.price,
        })
        return this.res.redirect('/all-sub-category');
        
    
     
    } catch (error) {
      console.log(error)
    }
  

  }


};
