 

module.exports = {


    friendlyName: 'Create',
  
  
    description: 'Create category.',
  
  
    inputs: {
  
    },
  
  
    exits: {
  
    },
  
  
    fn: async function (inputs,exits) {
      try {
        res = this.req.body
        let create_sub_category = await Sub_category.find().populate('category_name');
        return this.res.view('pages/all-sub-category', {
            user: res,
            sub_category: create_sub_category
        });
    
      } catch(error) {
          console.log(error)
      }
    }
  
  
  };
  