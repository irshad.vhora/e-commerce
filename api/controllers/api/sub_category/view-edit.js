 

module.exports = {


    friendlyName: '',
  
  
    description: '=',
  
  
    inputs: {
  
    },
  
  
    exits: {
  
    },
  
  
    fn: async function (inputs,exits) {
      try {
        let createCategory = await Category.find();
    
        let sub_details = await Sub_category.findOne({id:this.req.param('id')}).populate('category_name');

        return this.res.view('pages/edit_sub_category', {
            sub_category : sub_details,
            category_name: createCategory,
            
        }); 
    
      } catch(error) {
          console.log(error)
      }
    }
    
  
  
  };
  