module.exports = {


  friendlyName: 'Delete',


  description: 'Delete sub category.',


  inputs: {

  },


  exits: {

  },


  fn: async function (inputs) {

    try{

      const sub_category = this.req.body  

        var sub_categoryRecord = await Sub_category.destroyOne({
          id : this.req.param('id')
        });
        return this.res.redirect('/all-sub-category');
        
    
     
    } catch (error) {
      console.log(error)
    }

  }


};
