
module.exports = {


  friendlyName: 'Create',


  description: 'Create sub category.',


  inputs: {

  },


  exits: {

  },


  fn: async function (inputs,exits) {

    try {
      res = this.req.body
     
     // var img_url = await user.upload(res.photo)
      
      let create_sub_category = await Sub_category.create({
        category_name : res.category_name,
        name : res.name,
        model_details : res.model_details,
        description : res.description,
        photo : res.photo,
        price : res.price,
      }).fetch();

      return this.res.redirect('/category');
    } catch(error) {
        console.log(error)
    }
  }


};
