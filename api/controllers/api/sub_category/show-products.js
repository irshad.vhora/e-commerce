 

module.exports = {


    friendlyName: 'Create',
  
  
    description: 'Create category.',
  
  
    inputs: {
  
    },
  
  
    exits: {
  
    },
  
  
    fn: async function (inputs,exits) {
      try {
        res = this.req.body
        let create_sub_category = await Sub_category.find();
        
        return this.res.view('pages/show_products', {
            user: res,
            sub_category: create_sub_category
        });
    
      } catch(error) {
          console.log(error)
      }
    }
  
  
  };
  