//const Category = require("../../../models/Category");

module.exports = {


  friendlyName: 'Create',


  description: 'Create category.',


  inputs: {

  },


  exits: {

  },


  fn: async function (inputs,exits) {
    try {
      res = this.req.body
      user_id = this.req.session.user.id
      let createCategory = await Category.create({
        name : res.name,
        user_id : user_id
      }).fetch();

      return this.res.redirect('/sub_category');
    } catch(error) {
        console.log(error)
    }
  }


};
