//const Category = require("../../../models/Category");

module.exports = {


    friendlyName: 'Create',
  
  
    description: 'Create category.',
  
  
    inputs: {
  
    },
  
  
    exits: {
  
    },
  
  
    fn: async function (inputs,exits) {
      try {
  
        return this.res.view('pages/category',{
           locals: {layout: '/layouts/loginuser.ejs'}
        });
      } catch(error) {
          console.log(error)
      }
    }
  
  
  };
  