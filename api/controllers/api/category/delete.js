const User = require("../../../models/User");

module.exports = {


  friendlyName: 'Delete',


  description: 'Delete category.',


  inputs: {

  },


  exits: {

  },


  fn: async function (inputs) {

    try{
      const category = this.req.body  

        var categoryRecord = await Category.destroyOne({
          id : this.req.param('id')
        });
        return this.res.redirect('/all-category');
        
    
     
    } catch (error) {
      console.log(error)
    }
  }


};
