 

module.exports = {


    friendlyName: 'Create',
  
  
    description: 'Create category.',
  
  
    inputs: {
  
    },
  
  
    exits: {
  
    },
  
  
    fn: async function (inputs,exits) {
      try {
        res = this.req.body
        user_id = this.req.session.user.id;
        let createCategory = await Category.find({user_id : user_id});
       
        return this.res.view('pages/all-category', {
            
            category: createCategory
        });
    
      } catch(error) {
          console.log(error)
      }
    }
  
  
  };
  