module.exports = {


  friendlyName: 'Show p',


  description: '',


  inputs: {

  },


  exits: {

  },


  fn: async function (inputs,exits) {

    try {
      res = this.req.body
      let create_sub_category = await Sub_category.find({ category_name : this.req.param('id')}).populate('category_name');
      // return exits.success({
      //    data : create_sub_category
      // });
    if(!_.isEmpty(create_sub_category)){
    
      return this.res.view('pages/show_products', {
          category:await Category.findOne({id:this.req.param('id')}),
          user: res,
          sub_category: create_sub_category,
          locals: {layout: '/layouts/loginuser.ejs'}
      });
    }else{
      return this.res.view('pages/notfound',{
         category:await Category.findOne({id:this.req.param('id')}),
         message:"Data not found ! it will come later.",
         locals:{layout: '/layouts/loginuser.ejs'}
      })
    }
    } catch(error) {
        console.log(error)
    }
  }

};
