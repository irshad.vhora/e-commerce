//const Category = require("../../../models/Category");

module.exports = {


    friendlyName: 'Create',
  
  
    description: 'Create category.',
  
  
    inputs: {
  
    },
  
  
    exits: {
  
    },
  
  
    fn: async function (inputs,exits) {
        var category_id = this.req.param('id');
        
        var category_details = await Category.findOne({id:category_id});
        
        return this.res.view('pages/edit-category',{
            category : category_details,
            locals: {layout: '/layouts/loginuser.ejs'}
        });
     
    }
  
  
  };
  