const category2 = require("./category2");

module.exports = {


  friendlyName: 'Update',


  description: 'Update category.',


  inputs: {

  },


  exits: {

  },


  fn: async function (inputs) {

    try {
     
        var updatedCategory = await Category.updateOne({
          id: this.req.param('id'),
        }).set({
          name : this.req.body.name
        });

        return this.res.redirect('/all-category');

    } catch (error) {
      console.log(error)
    }
   

    
  

  }


};
