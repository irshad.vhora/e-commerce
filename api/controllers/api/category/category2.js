 

module.exports = {


    friendlyName: 'Create',
  
  
    description: 'Create category.',
  
  
    inputs: {
  
    },
  
  
    exits: {
  
    },
  
  
    fn: async function (inputs) {
      try {
        res = this.req.body
        let createCategory = await Category.find();
        // return ({
        //     data : createCategory
        // })
        return this.res.view('pages/sub_category', {
            
            category_name: createCategory
        });
    
      } catch(error) {
          console.log(error)
      }
    }
    
  
  
  };
  