module.exports = {


    friendlyName: '',
  
  
    description: '',
  
  
    inputs: {
  
    },
  
  
    exits: {
  
    },
  
  
    fn: async function (inputs) {
  
      try {
        res = this.req.body
        user_id = this.req.session.user.id;
       

        let create_order = await Order_details.create({
          user_id : user_id,
          sub_category_id : res.sub_category_id,
          customer_name : res.customer_name,
          mobile_no : res.mobile_no,
          address : res.address,
          price : res.price,
          payment_status : 1,
        }).fetch();
  
        return this.res.redirect('/api/show');
      } catch(error) {
          console.log(error)
      }
    }
  
  
  };
  