 

module.exports = {


    friendlyName: '',
  
  
    description: '=',
  
  
    inputs: {
  
    },
  
  
    exits: {
  
    },
  
  
    fn: async function () {
      try {
        let allCategory = await Category.find();
    
        let product_details = await Sub_category.findOne({id:this.req.param('id')}).populate('category_name')

        return this.res.view('pages/add-to-cart', {
            product : product_details,
            category_name: allCategory,
            
        }); 
    
      } catch(error) {
          console.log(error)
      }
    }
    
  
  
  };
  