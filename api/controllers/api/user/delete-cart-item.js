module.exports = {


    friendlyName: 'Delete',
  
  
    description: 'Delete cart items.',
  
  
    inputs: {
  
    },
  
  
    exits: {
  
    },
  
  
    fn: async function (inputs) {
  
      try{
  
        var cart_record = await Cart.destroyOne({
        id : this.req.param('id')
        });

        return this.res.redirect('/view-cart');
        
      } catch (error) {
        console.log(error)
      }
  
    }
}
  