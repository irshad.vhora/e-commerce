module.exports = {


  friendlyName: 'Login',


  description: 'Login user.',


  inputs: {
 
  },


  exits: {

  },


  fn: async function (req, res, env) {
    try {
      data = this.req.body
     // console.log(data.email)
      const user = await User.findOne({ email: data.email });
      if (_.isEmpty(user)) {
        return this.res.view("pages/login2",{
          error : data,
          message : "Your email is not registered with us! please sign up then you can login."
        });
      } else {
        await sails.helpers.passwords.checkPassword(data.password, user.password).intercept("incorrect", (error) => {
          return this.res.view("pages/login2",{
            error : data,
            message : "Password is wrong!"
          });
        });
        this.req.session.user = user
        this.res.redirect("/category-products");     
      } 
      

    } catch (error) {
      console.log(error)
    }
  }
};         
