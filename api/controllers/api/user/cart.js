//const Cart = require("../../../models/Cart");
//const Order_details = require("../../../models/Order_details");

module.exports = {


  friendlyName: 'Order details',


  description: '',


  inputs: {
     
  },


  exits: {

  },


  fn: async function (inputs) {
    try {
      
      res = this.req.body
      user_id = this.req.session.user.id;
      let cardCreate = await Cart.create({
        
        user_id : user_id,
        category_id : res.category_id,
        sub_category_id : res.sub_category_id,
        sub_category_name : res.sub_category_name,
        model_details : res.model_details,
        description : res.description,
        photo : res.photo,
        price : res.price,

      }).fetch();
      let usercart = await Cart.find({user_id : user_id});
      return this.res.redirect("/category-products")
      // return this.res.view("pages/view-ca{rt",{
      //    cart : usercart,
      //    locals:{layout: '/layouts/loginuser.ejs'}
      // })
    
    } catch (error) {
        console.log(error);
    }
  }

};
