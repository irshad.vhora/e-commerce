module.exports = {


  friendlyName: 'Logout',


  description: 'Logout user.',


  inputs: {

  },


  exits: {

  },


  fn: async function (inputs) {

   
    delete (this.req.session.user);
   
    
    // return this.res.redirect("/login");
    return this.res.view("pages/index",{
      locals: {layout: '/layouts/loginuser.ejs'}
    });

  }


};
