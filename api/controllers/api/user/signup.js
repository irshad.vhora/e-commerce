const { Sails } = require("sails");
const sailsHookGrunt = require("sails-hook-grunt");

module.exports = {


  friendlyName : 'Signup',


  description : 'Signup user.',


  inputs : {

  },


  exits : {

  },


  fn: async function (res,exits) {

    try {
      
      res = this.req.body
      let userCreate = await User.create({
        
        first_name : res.first_name,
        last_name : res.last_name,
        email : res.email,
        phone : res.phone,
        password : res.password,
      }).fetch();
     
      return this.res.redirect('/login');
    
    } catch (error) {
        console.log(error);
    }

  }


};
