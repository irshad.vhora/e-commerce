//const Cart = require("../../../models/Cart");

 

module.exports = {


    friendlyName: '',
  
  
    description: '',
  
  
    inputs: {
  
    },
  
  
    exits: {
  
    },
  
  
    fn: async function (inputs,exits) {
      try {
         var user = this.req.session.user;
         let view_cart = await Cart.find({
             user_id : user.id,
         });
        
         return this.res.view('pages/view-cart', {
             
             cart : view_cart
         });
       
    
      } catch(error) {
          console.log(error)
      }
    }
  
  
  };
  