/**
 * Order_details.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    user_id : {
      model:"User"
    },
    sub_category_id:{
      model:"Sub_category"
    },
    customer_name : {
      type:"string",
      required:true
    },

    address : {
      type:"string",
      required:true
    },
    price : {
      type : 'number',
      required : true,
    },
    payment_status : {
      type : 'number',
      required : true,
    },
   
    mobile_no : {
      type : 'number',
      required : true
    }
  },

};

