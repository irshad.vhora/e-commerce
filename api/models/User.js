/**
 * User.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {

    phone : {
      type : 'number',
      required : true,
    },

    email : {
      type : 'string',
      required : true,
      unique : true,
      isEmail : true,
    },

    first_name : {
      type : 'string',
      required : true,
    },

    last_name : {
      type : 'string',
      required : true,
    },
   
    password : {
      type : 'string',
      required : true
    },

    role : {
      type : "string",
      isIn : ["admin", "user"],
      required : false,
      defaultsTo : "user"
    }

  },

  customToJSON: function () {
    return _.omit(this, ["password"]);
  },

  beforeCreate: async function (values, proceed) {
    const hashedPassword = await sails.helpers.passwords.hashPassword(
      values.password
    );
    values.password = hashedPassword;
    return proceed();
  },

};

