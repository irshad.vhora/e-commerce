/**
 * Cart.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    user_id : {
      model:"User"
    },
    category_id : {
      model : "Category",
    },
    sub_category_id:{
      model:"Sub_category"
    },
    sub_category_name : {
      type:"string",
      required:true
    },

    model_details : {
      type:"string",
      required:false
    },

    description : {
      type : 'string',
      required : false,
    },
    photo : {
      type : 'string',
      required : false,
    },
    price : {
      type : 'number',
      required : false
    }
  },

};

