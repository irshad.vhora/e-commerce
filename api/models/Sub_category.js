/**
 * Sub_category.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {

    category_name : {
      model : 'Category',
    },

    name : {
      type : 'string',
      required : true,
    },

    model_details : {
      type : 'string',
      required : true,
    },

    description : {
      type : 'string',
      required : true,
    },
   
    photo : {
      type : 'string',
      required : true
    },

    price : {
      type : "string",
      required : true
    }

  },

};

